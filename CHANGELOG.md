
## 0.0.24 [01-03-2024]

* Remove img tag from markdown file

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!42

---

## 0.0.23 [10-19-2023]

* add deprecation information

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!40

---

## 0.0.22 [07-18-2022]

* Patch/dsup 1361

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!38

---

## 0.0.21 [02-14-2022]

* Patch/dsup 1046

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!36

---

## 0.0.20 [07-28-2021]

* updated app-artifacts version requirement

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!35

---

## 0.0.19 [07-01-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!33

---

## 0.0.18 [05-12-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!32

---

## 0.0.17 [04-06-2021]

* Update Push Bundles to Gitlab.json

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!28

---

## 0.0.16 [03-12-2021]

* Update bundles/transformations/getArtifactPath.json

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!27

---

## 0.0.15 [03-10-2021]

* Update /bundles/transformations/CompliancefilterArrayOfObjects.json,...

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!22

---

## 0.0.14 [03-02-2021]

* handle scalability issues

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!20

---

## 0.0.13 [03-02-2021]

* Added SOC2 compliance logic

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!12

---

## 0.0.12 [02-09-2021]

* updated JST to support non standard scopes

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!11

---

## 0.0.11 [02-02-2021]

* Update package.json, README.md, bundles/workflows/Push Bundles to Gitlab Commit Data.json files

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!8

---

## 0.0.10 [11-30-2020]

* Updated 'constract' to 'construct'

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab!7

---

## 0.0.9 [11-02-2020]

* [LB-497] patch/README.md

See merge request itentialopensource/pre-built-automations/staging/push-bundles-to-gitlab!4

---

## 0.0.8 [10-22-2020]

* [LB-497] patch/README.md

See merge request itentialopensource/pre-built-automations/staging/push-bundles-to-gitlab!4

---

## 0.0.7 [10-16-2020]

* removed 2 json forms from manifest

See merge request itentialopensource/pre-built-automations/staging/push-bundles-to-gitlab!3

---

## 0.0.6 [10-15-2020]

* Update bundles/json_forms/Push Bundles to Gitlab.json, package.json files

See merge request itentialopensource/pre-built-automations/staging/push-bundles-to-gitlab!2

---

## 0.0.5 [10-15-2020]

* Update bundles/json_forms/Push Bundles to Gitlab.json, package.json files

See merge request itentialopensource/pre-built-automations/staging/push-bundles-to-gitlab!2

---

## 0.0.4 [10-15-2020]

* Patch/merge all change

See merge request itentialopensource/pre-built-automations/staging/push-bundles-to-gitlab!1

---

## 0.0.3 [10-05-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.2 [10-05-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
